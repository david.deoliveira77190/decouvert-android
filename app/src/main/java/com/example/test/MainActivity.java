package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Test Start", "Méthode onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("Test Stop", "Méthode onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Test Destroy", "Méthode onDestroy");
    }

}